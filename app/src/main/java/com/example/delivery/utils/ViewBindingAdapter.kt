package com.example.delivery.utils

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.view.ContextThemeWrapper
import android.view.View
import android.widget.EditText
import androidx.core.widget.addTextChangedListener
import androidx.databinding.BindingAdapter
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.delivery.entities.OrderInfo
import com.example.delivery.ui.order.MainActivity
import com.example.delivery.ui.order.details.DetailsActivity

/**
 * Created by Mohamed Fadel
 * Date: 10/28/2019.
 * email: mohamedfadel91@gmail.com.
 */
@BindingAdapter("goneUnless")
fun goneUnless(view: View, visible: Boolean) {
    view.visibility = if (visible) View.VISIBLE else View.GONE
}

@BindingAdapter("onRefresh")
fun onRefresh(view: SwipeRefreshLayout, func: () -> Unit) {
    view.setOnRefreshListener {
        func.invoke()
    }
}

@BindingAdapter("dismissRefresh")
fun dismissRefresh(view: SwipeRefreshLayout, visible: Boolean) {
    view.isRefreshing = visible
}

@BindingAdapter("phonenum")
fun callPhoneNum(view: View, num: String) {
    view.setOnClickListener {
        view.context?.handlePhoneCall(num)
    }

}

@BindingAdapter("disableUnless")
fun disableUnless(view: View, enable: Boolean) {
    view.isEnabled = enable
}

@BindingAdapter("whatsAppNum")
fun openwhatsAppChat(view: View, num: String) {
    view.setOnClickListener {
        var number: String
        try {
            number = num.substring(num.indexOf("01"))
        }catch (ex: Exception){
            number = num
        }
        view.context?.startActivity(Intent().apply {
            action = Intent.ACTION_VIEW
            data = Uri.parse("https://wa.me/2$number")
        })
    }

}


@BindingAdapter(value = ["confirmTitle", "onConfirm"], requireAll = false)
fun confirmBefore(view: View, title: String, func: Runnable) {
    view.setOnClickListener {
        view.context?.showConfirmationDialog(title) {
            func.run()
        }
    }
}


@BindingAdapter("navToDetails")
fun navigateToDetails(view: View, info: OrderInfo) {
    print(info)
    view.setOnClickListener {
        ((it.context as? ContextThemeWrapper)?.baseContext as? Activity)?.startActivityForResult(
            DetailsActivity.buildIntent(it.context!!, info),
            MainActivity.REQUEST_DETAILS_CODE
        )
    }

}

interface textChangeListener{
    fun changeText(text: String)
}
@BindingAdapter("onTextWatcher")
fun watchTextChange(editText: EditText, runnable: textChangeListener) {
    editText.addTextChangedListener {
        runnable.changeText(it.toString())
    }

}

