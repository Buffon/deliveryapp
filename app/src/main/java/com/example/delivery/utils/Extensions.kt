package com.example.delivery.utils

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Parcel
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.core.os.ParcelCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.*
import com.example.delivery.BuildConfig
import com.example.delivery.R
import com.example.delivery.entities.Location
import com.example.delivery.result.Event
import com.example.delivery.result.EventObserver
import com.example.delivery.result.Result
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import es.dmoral.toasty.Toasty
import timber.log.Timber
import java.security.MessageDigest
import java.text.SimpleDateFormat
import java.util.*
import kotlin.text.Charsets.UTF_8

/** Convenience for callbacks/listeners whose return value indicates an event was consumed. */
inline fun consume(f: () -> Unit): Boolean {
    f()
    return true
}

/**
 * Allows calls like
 *
 * `viewGroup.inflate(R.layout.foo)`
 */
fun ViewGroup.inflate(@LayoutRes layout: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layout, this, attachToRoot)
}

/**
 * Allows calls like
 *
 * `supportFragmentManager.inTransaction { add(...) }`
 */
inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> FragmentTransaction) {
    beginTransaction().func().commit()
}

// region ViewModels

/**
 * For Actvities, allows declarations like
 * ```
 * val myViewModel = viewModelProvider(myViewModelFactory)
 * ```
 */
inline fun <reified VM : ViewModel> FragmentActivity.viewModelProvider(
    provider: ViewModelProvider.Factory
) =
    ViewModelProviders.of(this, provider).get(VM::class.java)

/**
 * For Fragments, allows declarations like
 * ```
 * val myViewModel = viewModelProvider(myViewModelFactory)
 * ```
 */
inline fun <reified VM : ViewModel> Fragment.viewModelProvider(
    provider: ViewModelProvider.Factory
) =
    ViewModelProviders.of(this, provider).get(VM::class.java)

/**
 * Like [Fragment.viewModelProvider] for Fragments that want a [ViewModel] scoped to the Activity.
 */
inline fun <reified VM : ViewModel> Fragment.activityViewModelProvider(
    provider: ViewModelProvider.Factory
) =
    ViewModelProviders.of(requireActivity(), provider).get(VM::class.java)

/**
 * Like [Fragment.viewModelProvider] for Fragments that want a [ViewModel] scoped to the parent
 * Fragment.
 */
inline fun <reified VM : ViewModel> Fragment.parentViewModelProvider(
    provider: ViewModelProvider.Factory
) =
    ViewModelProviders.of(parentFragment!!, provider).get(VM::class.java)

// endregion
// region Parcelables, Bundles

/** Write a boolean to a Parcel. */
fun Parcel.writeBooleanUsingCompat(value: Boolean) = ParcelCompat.writeBoolean(this, value)

/** Read a boolean from a Parcel. */
fun Parcel.readBooleanUsingCompat() = ParcelCompat.readBoolean(this)

// endregion
// region LiveData

/** Uses `Transformations.map` on a LiveData */
fun <X, Y> LiveData<X>.map(body: (X) -> Y): LiveData<Y> {
    return Transformations.map(this, body)
}

fun <X, Y> LiveData<X>.switchMap(body: (X) -> LiveData<Y>): LiveData<Y> {
    return Transformations.switchMap(this, body)
}

fun <T> MutableLiveData<T>.setValueIfNew(newValue: T) {
    if (this.value != newValue) value = newValue
}

fun <T> MediatorLiveData<Event<String>>.observeErrorFrom(source: LiveData<Result<T>>) {
    this.addSource(source) {
        if (it is Result.Error) {
            this.postValue(Event(it.errorMessage))
        }
    }
}

fun Context.handlePhoneCall(num: String) {
    startActivity(Intent().apply {
        action = Intent.ACTION_DIAL
        data = Uri.parse("tel: $num")
    })
}

fun String.generateSHA256Hash(): String? {
    try {
        val digest = MessageDigest.getInstance("SHA-256");
        val hash = digest.digest(this.toByteArray(UTF_8));
        return String(hash)
    } catch (e: Exception) {
        e.printStackTrace();
    }
    return null;
}

fun Date.toFormattedString(): String {
    var dateText = ""
    if (DateUtils.isToday(time)) {
        dateText = "Today "
    }

    val formatter = SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH)

    var string = ""
    try {
        string += formatter.format(this)
    } catch (ex: Exception) {

    }

    dateText += string
    return dateText
}

fun Date.isTheSameDay(date: Date?): Boolean {
    if (date == null) return false
    val cal1 = Calendar.getInstance().apply {
        time = this@isTheSameDay
    }
    val cal2 = Calendar.getInstance().apply {
        time = date
    }

    return cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR) &&
            cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR);

}


fun Context.startNavigationTo(location: Location) {
    val gmmIntentUri = Uri.parse("google.navigation:q=${location.latitude},${location.longitude}")
    val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
    mapIntent.setPackage("com.google.android.apps.maps")
    startActivity(mapIntent)
}

fun Context.showConfirmationDialog(title: String, positiveAction: () -> Unit) {
    AlertDialog.Builder(this)
        .setTitle(title)
        .setMessage(R.string.confirm_msg)
        .setPositiveButton(R.string.yes) { dialog: DialogInterface, i: Int ->
            positiveAction.invoke()
            dialog.dismiss()
        }
        .setNegativeButton(R.string.no) { dialog: DialogInterface, i: Int ->
            dialog.dismiss()
        }
        .show()
}

fun GoogleMap.animateZoomToMyLocation(zoomLevel: Float = 16F) {
    animateCamera(
        CameraUpdateFactory.newLatLngZoom(
            LatLng(
                myLocation.latitude,
                myLocation.longitude
            ), zoomLevel
        )
    )
}

fun LiveData<Event<String>>.showErrorMessage(activity: FragmentActivity) {
    this.observe(activity, EventObserver {
        Toasty.error(activity, it, Toast.LENGTH_SHORT).show()
    })
}

fun LiveData<Event<String>>.showSuccessMessage(activity: FragmentActivity) {
    this.observe(activity, EventObserver {
        Toasty.success(activity, it, Toast.LENGTH_SHORT).show()
    })
}

fun LiveData<Event<String>>.showInfoMessage(activity: FragmentActivity) {
    this.observe(activity, EventObserver {
        Toasty.info(activity, it, Toast.LENGTH_SHORT).show()
    })
}


fun <T, E> MediatorLiveData<E>.observeDataFrom(
    source: LiveData<Result<T>>,
    preProcessing: ((T) -> E)? = null
) {
    this.addSource(source) {
        if (it is Result.Success) {
            val result = if (preProcessing != null) preProcessing(it.data) else it.data
            this.postValue(result as E)
        }
    }
}


/**
 * Helper to force a when statement to assert all options are matched in a when statement.
 *
 * By default, Kotlin doesn't care if all branches are handled in a when statement. However, if you
 * use the when statement as an expression (with a value) it will force all cases to be handled.
 *
 * This helper is to make a lightweight way to say you meant to match all of them.
 *
 * Usage:
 *
 * ```
 * when(sealedObject) {
 *     is OneType -> //
 *     is AnotherType -> //
 * }.checkAllMatched
 */
val <T> T.checkAllMatched: T
    get() = this

// region UI utils

// endregion

/**
 * Helper to throw exceptions only in Debug builds, logging a warning otherwise.
 */
fun exceptionInDebug(t: Throwable) {
    if (BuildConfig.DEBUG) {
        throw t
    } else {
        Timber.e(t)
    }
}
