package com.example.delivery.api

import androidx.lifecycle.LiveData
import com.example.delivery.entities.OrderAction
import com.example.delivery.entities.OrderInfo
import com.example.delivery.entities.OrderItem
import com.example.delivery.entities.OrderStatus
import com.example.delivery.repo.ApiResponse
import com.example.delivery.repo.ResponseWrapper
import com.example.delivery.repo.order.OrderApi
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Query
import javax.inject.Inject

/**
 * Created by Mohamed Fadel
 * Date: 10/27/2019.
 * email: mohamedfadel91@gmail.com.
 */
interface OrderApiSpec {
    @GET("xxx/YYYY1")
    fun getOrderStatusList(): LiveData<ApiResponse<ResponseWrapper<List<OrderStatus>>>>

    @GET("xxx/YYYY2")
    fun getOrderList(@Query("param1") param1: String?, @Query("param2") param2: Int): LiveData<ApiResponse<ResponseWrapper<List<OrderInfo>>>>

    @GET("xxx/YYYY3")
    fun getOrderItems(@Query("param1") param1: String?, @Query("param2") param2: Int): LiveData<ApiResponse<ResponseWrapper<List<OrderItem>>>>

    @GET("xxx/YYYY4")
    fun getOrderActions(@Query("param1") param1: Int): LiveData<ApiResponse<ResponseWrapper<List<OrderStatus>>>>

    @PUT("xxx/YYYY5")
    fun executeActionOnOrder(@Query("param1") param1: String?, @Body orderAction: OrderAction): LiveData<ApiResponse<ResponseWrapper<Any>>>
}

class RetrofitOrderApi @Inject constructor(private val orderApiSpec: OrderApiSpec) : OrderApi {
    override fun getOrderList(auth: String?, orderStatus: Int): LiveData<ApiResponse<ResponseWrapper<List<OrderInfo>>>> {
        return orderApiSpec.getOrderList(auth, param2 = orderStatus)
    }

    override fun getOrderStatusList(): LiveData<ApiResponse<ResponseWrapper<List<OrderStatus>>>> {
        return orderApiSpec.getOrderStatusList()
    }

    override fun getOrderItemList(auth: String?, orderId: Int): LiveData<ApiResponse<ResponseWrapper<List<OrderItem>>>> {
        return orderApiSpec.getOrderItems(auth, param2 = orderId)
    }

    override fun getOrderActionList(orderId: Int): LiveData<ApiResponse<ResponseWrapper<List<OrderStatus>>>> {
        return orderApiSpec.getOrderActions(orderId)
    }

    override fun executeOrderAction(
        auth: String?,
        orderId: Int,
        orderStatus: Int
    ): LiveData<ApiResponse<ResponseWrapper<Any>>> {
        return orderApiSpec.executeActionOnOrder(auth, orderAction = OrderAction(orderId, orderStatus))
    }
}