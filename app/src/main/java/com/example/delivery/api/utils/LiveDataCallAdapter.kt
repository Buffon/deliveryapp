package com.example.delivery.api.utils

import androidx.lifecycle.LiveData
import com.example.delivery.repo.ApiResponse
import com.example.delivery.repo.ResponseWrapper
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Callback
import retrofit2.Response
import java.lang.reflect.Type
import java.util.concurrent.atomic.AtomicBoolean

/**
 * A Retrofit adapter that converts the Call into a LiveData of ApiResponse.
 * @param <R>
</R> */
class LiveDataCallAdapter<R, T>(private val responseType: Type) :
    CallAdapter<R, LiveData<ApiResponse<ResponseWrapper<T>>>> {

    override fun responseType() = responseType

    override fun adapt(call: Call<R>): LiveData<ApiResponse<ResponseWrapper<T>>> {
        return object : LiveData<ApiResponse<ResponseWrapper<T>>>() {
            private var started = AtomicBoolean(false)
            override fun onActive() {
                super.onActive()
                if (started.compareAndSet(false, true)) {
                    call.enqueue(object : Callback<R> {
                        override fun onResponse(call: Call<R>, response: Response<R>) {
                            val resp: Response<ResponseWrapper<T>> = response as Response<ResponseWrapper<T>>
                            postValue(ApiResponse.create(resp))
                        }

                        override fun onFailure(call: Call<R>, throwable: Throwable) {
                            postValue(ApiResponse.create(throwable))
                        }
                    })
                }
            }
        }
    }
}
