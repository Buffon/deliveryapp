package com.example.delivery.api

import androidx.lifecycle.LiveData
import com.example.delivery.entities.Location
import com.example.delivery.repo.ApiResponse
import com.example.delivery.repo.ResponseWrapper
import com.example.delivery.repo.address.AddressApi
import retrofit2.http.Body
import retrofit2.http.PUT
import retrofit2.http.Query
import javax.inject.Inject

/**
 * Created by Mohamed Fadel
 * Date: 10/27/2019.
 * email: mohamedfadel91@gmail.com.
 */
interface AddressApiSpec {
    @PUT("xxx/yyy")
    fun updateAddressLocation(@Body location: Location, @Query("param1") param1: Int): LiveData<ApiResponse<ResponseWrapper<Any>>>
}

class RetrofitAddressApi @Inject constructor(private val addressApiSpec: AddressApiSpec) :
    AddressApi {
    override fun updateAddressLocation(
        addressId: Int,
        location: Location
    ): LiveData<ApiResponse<ResponseWrapper<Any>>> {
        return addressApiSpec.updateAddressLocation(location, addressId)
    }
}