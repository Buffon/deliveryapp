package com.example.delivery.ui.order.details


import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.delivery.BR
import com.example.delivery.R
import com.example.delivery.databinding.FragmentOrderDetailsBinding
import com.example.delivery.databinding.OrderItemListBinding
import com.example.delivery.entities.OrderItem
import com.example.delivery.result.EventObserver
import com.example.delivery.ui.location.LocationActivity
import com.example.delivery.utils.showConfirmationDialog
import com.example.delivery.utils.showErrorMessage
import com.example.delivery.utils.viewModelProvider
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_order_details.*
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class OrderDetailsFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var orderItemsViewModel: OrderItemsViewModel
    private lateinit var orderActionsViewModel: OrderActionsViewModel

    private lateinit var binding: FragmentOrderDetailsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        orderItemsViewModel = viewModelProvider(viewModelFactory)
        orderActionsViewModel = viewModelProvider(viewModelFactory)

        binding = FragmentOrderDetailsBinding.inflate(inflater, container, false).apply {
            viewModel = orderItemsViewModel
            actionsViewModel = orderActionsViewModel
            lifecycleOwner = viewLifecycleOwner
        }

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        orderItemsViewModel.loadOrderItemListResult.observe(this, Observer {
            if (list.adapter == null) {
                list.adapter = OrderItemListAdapter(context!!)
            }
            (list.adapter as OrderItemListAdapter).apply {
                submitList(it)
            }
        })

        orderActionsViewModel.navigateTo.observe(this, EventObserver {
            when (it.dest) {
                Destination.LOCATION -> {
                    startActivityForResult(
                        Intent(context!!, LocationActivity::class.java).apply {
                            putExtras(it.extras!!)
                        },
                        LOCATION_REQUEST_CODE and 0x0000ffff
                    )
                }
                Destination.BACK -> {
                    activity!!.setResult(RESULT_OK)
                    activity!!.finish()
                }

            }
        })

        orderItemsViewModel.errorMessage.showErrorMessage(activity!!)
        orderActionsViewModel.errorMessage.showErrorMessage(activity!!)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == LOCATION_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                activity!!.showConfirmationDialog(getString(R.string.delivered_warning)) {
                    orderActionsViewModel.onDeliveredButtonDialogClicked()
                }
            }
        }
    }


    companion object {
        const val LOCATION_REQUEST_CODE = 0x01
    }
}

class OrderItemListAdapter(
    private val context: Context
) : ListAdapter<OrderItem, OrderItemListAdapter.OrderItemViewHolder>(OrderItemDiff) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderItemViewHolder {
        return OrderItemViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context), viewType,
                parent, false
            )
        )
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.order_item_list
    }

    override fun onBindViewHolder(holder: OrderItemViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class OrderItemViewHolder(private val binding: OrderItemListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: OrderItem) {
            binding.setVariable(BR.orderItem, item)
        }

    }
}

object OrderItemDiff : DiffUtil.ItemCallback<OrderItem>() {
    override fun areItemsTheSame(oldItem: OrderItem, newItem: OrderItem): Boolean {
        return oldItem.product_name == newItem.product_name
    }

    override fun areContentsTheSame(oldItem: OrderItem, newItem: OrderItem) = oldItem == newItem
}
