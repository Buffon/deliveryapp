package com.example.delivery.ui.order

import com.example.delivery.di.scope.FragmentScoped
import com.example.delivery.ui.order.list.OrderListFragment
import com.example.delivery.ui.order.list.OrderListModule
import com.example.delivery.ui.order.status.OrderStatusListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by Mohamed Fadel
 * Date: 10/27/2019.
 * email: mohamedfadel91@gmail.com.
 */
@Module
internal abstract class MainModule {

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun contributeOrderStatusListFragment(): OrderStatusListFragment

    @FragmentScoped
    @ContributesAndroidInjector(modules = [OrderListModule::class])
    internal abstract fun contributeOrderListFragment(): OrderListFragment


}