package com.example.delivery.ui.login

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.delivery.databinding.ActivityLoginBinding
import com.example.delivery.R
import com.example.delivery.result.EventObserver
import com.example.delivery.ui.order.MainActivity
import com.example.delivery.utils.viewModelProvider
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class LoginActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val loginViewModel: LoginViewModel = viewModelProvider(viewModelFactory)

        DataBindingUtil.setContentView<ActivityLoginBinding>(this, R.layout.activity_login).apply {
            viewModel = loginViewModel
        }

        loginViewModel.navigateTo.observe(this, EventObserver {
            when (it) {
                Destination.MAIN -> startActivity(Intent(this, MainActivity::class.java))
            }
            finish()
        })
    }
}
