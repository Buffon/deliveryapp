package com.example.delivery.ui.location

import android.os.Handler
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.example.delivery.utils.animateZoomToMyLocation
import com.google.android.gms.maps.GoogleMap

class MapInitialZoomToCurrentLocationHandler(
    private val maps: GoogleMap,
    private val zoomLevel: Float = 16F
) : LifecycleObserver {
    private var userChangeMapsCamera: Boolean = false
    private var listenToCamera: Boolean = false
    private val handler = Handler()
    private val backoff: Long = 150

    init {
        this.maps.setOnMapLoadedCallback {
            if (!this.listenToCamera) {
                this.maps.setOnCameraChangeListener {
                    this.userChangeMapsCamera = true
                }
                this.listenToCamera = true
            }

            scheduleZoom()

        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun forceStop() {
        handler.removeCallbacksAndMessages(null)
    }

    private fun scheduleZoom() {
        handler.post(object : Runnable {
            override fun run() {
                if (!userChangeMapsCamera) {
                    if (maps.myLocation == null) {
                        handler.postDelayed(this, backoff)
                    } else {
                        maps.animateZoomToMyLocation(zoomLevel)
                    }

                }
            }
        })

    }
}