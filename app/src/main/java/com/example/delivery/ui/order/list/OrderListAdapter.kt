package com.example.delivery.ui.order.list

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.delivery.BR
import com.example.delivery.R
import com.example.delivery.databinding.OrderItemBinding
import com.example.delivery.databinding.OrdersHeaderItemBinding
import com.example.delivery.entities.OrderInfo
import com.example.delivery.utils.exceptionInDebug
import com.example.delivery.utils.isTheSameDay
import java.util.*

/**
 * Created by Mohamed Fadel
 * Date: 11/1/2019.
 * email: mohamedfadel91@gmail.com.
 */
class OrderListAdapter(
    private val isHeaderShown: Boolean
) : ListAdapter<Any, RecyclerView.ViewHolder>(OrderDiff) {

    companion object {
        private const val VIEW_TYPE_HEADING = R.layout.order_item_list
        private const val VIEW_TYPE_ORDER = R.layout.order_item

        private fun insertDayHeadings(list: List<OrderInfo>?): List<Any> {
            val newList = mutableListOf<Any>()

            var previousDate: Date? = null
            list?.forEach {
                val date = it.orderTime ?: return@forEach
                if (!date.isTheSameDay(previousDate)) {
                    newList += SectionHeader(
                        date
                    )
                }
                newList.add(it)
                previousDate = date
            }
            return newList
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            VIEW_TYPE_HEADING -> createHeadingViewHolder(parent)
            VIEW_TYPE_ORDER -> createOrderItemViewHolder(parent)
            else -> throw IllegalArgumentException("Unknown item type")
        }
    }

    private fun createHeadingViewHolder(parent: ViewGroup): HeadingViewHolder {
        return HeadingViewHolder(
            OrdersHeaderItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    private fun createOrderItemViewHolder(parent: ViewGroup): OrderItemViewHolder {
        return OrderItemViewHolder(
            OrderItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun submitList(list: MutableList<Any>?) {
        exceptionInDebug(
            RuntimeException("call `submitOrderInfoList()` instead to add day headings.")
        )
        super.submitList(list)
    }


    /** Prefer this method over [submitList] to add category headings. */
    fun submitOrderInfoList(list: List<OrderInfo>?) {
        super.submitList(if(isHeaderShown) insertDayHeadings(list) else list)
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is SectionHeader -> VIEW_TYPE_HEADING
            is OrderInfo -> VIEW_TYPE_ORDER
            else -> throw IllegalArgumentException("Unknown item type")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is HeadingViewHolder -> holder.bind(getItem(position) as SectionHeader)
            is OrderItemViewHolder -> holder.bind(getItem(position) as OrderInfo)
        }
    }

    class HeadingViewHolder(private val binding: OrdersHeaderItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(header: SectionHeader) {
            binding.setVariable(BR.header, header)
        }

    }

    class OrderItemViewHolder(private val binding: OrderItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(info: OrderInfo) {
            binding.setVariable(BR.orderInfo, info)
        }

    }
}

object OrderDiff : DiffUtil.ItemCallback<Any>() {
    override fun areItemsTheSame(oldItem: Any, newItem: Any): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Any, newItem: Any): Boolean {
        return (oldItem as? OrderInfo)?.order?.id?.equals((newItem as? OrderInfo)?.order?.id)
            ?: false;
    }
}