package com.example.delivery.ui.order.details

import androidx.lifecycle.ViewModel
import com.example.delivery.di.ViewModelKey
import com.example.delivery.di.scope.ActivityScoped
import com.example.delivery.entities.Order
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Created by Mohamed Fadel
 * Date: 11/5/2019.
 * email: mohamedfadel91@gmail.com.
 */

@Module
internal abstract class OrderActionsModule {

    @ActivityScoped
    @Binds
    @IntoMap
    @ViewModelKey(OrderActionsViewModel::class)
    internal abstract fun bindOrderActionsViewModel(viewModel: OrderActionsViewModel): ViewModel
}