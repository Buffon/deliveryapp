package com.example.delivery.ui

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.lifecycle.ViewModelProvider
import com.example.delivery.result.EventObserver
import com.example.delivery.ui.login.LoginActivity
import com.example.delivery.ui.order.MainActivity
import com.example.delivery.utils.checkAllMatched
import com.example.delivery.utils.viewModelProvider
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class LauncherActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var launchViewModel: LaunchViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        launchViewModel = viewModelProvider(viewModelFactory)
        launchViewModel.navigateTo.observe(this, EventObserver { destination ->
            when (destination) {
                Destination.MAIN -> startActivity(Intent(this, MainActivity::class.java))
                Destination.LOGIN -> startActivity(Intent(this, LoginActivity::class.java))
                Destination.EXIT -> Unit
            }.checkAllMatched
            finish()
        })


        ActivityCompat.requestPermissions(this, permissions, PERMISSIONS_REQUEST_CODE)

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 0x01) {
            for (i in permissions.indices) {
                if (grantResults.get(i) != PackageManager.PERMISSION_GRANTED) {
                    launchViewModel.OnNotAllPermissionsGranted()
                    return
                }
            }
            launchViewModel.OnAllPermissionsGranted()

        }
    }

    companion object {
        val permissions = arrayOf(
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
        const val PERMISSIONS_REQUEST_CODE = 0x01;
    }
}
