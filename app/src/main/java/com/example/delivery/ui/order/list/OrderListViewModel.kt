package com.example.delivery.ui.order.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.delivery.domain.orders.list.GetOrderInfoListUseCase
import com.example.delivery.entities.OrderStatus
import com.example.delivery.entities.OrderStatusEnum
import com.example.delivery.result.Event
import com.example.delivery.result.Result
import com.example.delivery.utils.map
import com.example.delivery.utils.observeDataFrom
import com.example.delivery.utils.observeErrorFrom
import com.example.delivery.utils.switchMap
import javax.inject.Inject

/**
 * Created by Mohamed Fadel
 * Date: 10/28/2019.
 * email: mohamedfadel91@gmail.com.
 */
class OrderListViewModel @Inject constructor(private val GetOrderListUseCase: GetOrderInfoListUseCase) :
    ViewModel() {

    private val orderStatus = MutableLiveData<OrderStatus>()

    private val loadOrderInfoList =
        orderStatus.switchMap { status ->
            GetOrderListUseCase.execute(status)
            GetOrderListUseCase.observe()
        }

    val loading: LiveData<Boolean>

    val isEmpty: LiveData<Boolean>

    private val _errorMessage = MediatorLiveData<Event<String>>()

    val errorMessage: LiveData<Event<String>>
        get() = _errorMessage

    private val _loadOrderInfoListResult = MediatorLiveData<OrderInfoItem>()

    val loadOrderInfoListResult: LiveData<OrderInfoItem>
        get() = _loadOrderInfoListResult


    init {

        loading = loadOrderInfoList.map { it is Result.Loading }
        isEmpty = loadOrderInfoList.map {
            it !is Result.Loading && ((it as? Result.Success)?.data.isNullOrEmpty() ?: true)
        }
        _errorMessage.observeErrorFrom(loadOrderInfoList)
        _loadOrderInfoListResult.observeDataFrom(loadOrderInfoList){
            OrderInfoItem(orderInfos = it, isHeaderShown =
            !it.isEmpty() && it.get(0)?.order?.orderStatus?.value == OrderStatusEnum.NEW.value)
        }

    }

    fun onScreenStarted(orderStatus: OrderStatus) {
        updateOrderInfoStatus(orderStatus)
    }

    fun onRefresh() {
        updateOrderInfoStatus(orderStatus.value)
    }

    private fun updateOrderInfoStatus(orderStatus: OrderStatus?) {
        if (orderStatus != null)
            this.orderStatus.postValue(orderStatus)
    }
}