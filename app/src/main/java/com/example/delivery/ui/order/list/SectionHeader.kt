package com.example.delivery.ui.order.list

import java.util.*

data class SectionHeader(
    val date: Date?
)
