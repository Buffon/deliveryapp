package com.example.delivery.ui.order.details

import androidx.lifecycle.ViewModel
import com.example.delivery.di.ViewModelKey
import com.example.delivery.di.scope.ActivityScoped
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Created by Mohamed Fadel
 * Date: 10/28/2019.
 * email: mohamedfadel91@gmail.com.
 */

@Module
internal abstract class OrderItemsModule {

    @ActivityScoped
    @Binds
    @IntoMap
    @ViewModelKey(OrderItemsViewModel::class)
    internal abstract fun bindOrderItemsViewModel(viewModel: OrderItemsViewModel): ViewModel
}