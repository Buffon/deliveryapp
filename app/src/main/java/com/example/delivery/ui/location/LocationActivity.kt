package com.example.delivery.ui.location

import android.app.Activity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.example.delivery.R
import com.example.delivery.result.EventObserver
import com.example.delivery.utils.checkAllMatched
import com.example.delivery.utils.showErrorMessage
import com.example.delivery.utils.showSuccessMessage
import com.example.delivery.utils.viewModelProvider
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class LocationActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_location)

        val viewModel: LocationViewModel = viewModelProvider(viewModelFactory)

        viewModel.navigateTo.observe(this, EventObserver {
            when (it) {
                Destination.DETAILS -> setResult(Activity.RESULT_OK)
            }.checkAllMatched
            finish()
        })

        viewModel.errorMessage.showErrorMessage(this)
        viewModel.updateLocationResult.showSuccessMessage(this)
    }

    companion object {

        const val EXTRA_ADDRESS_ID = "addressId"
        const val EXTRA_OLD_LOCATION = "oldLocation"
    }
}
