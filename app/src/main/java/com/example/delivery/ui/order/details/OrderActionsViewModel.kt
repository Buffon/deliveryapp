package com.example.delivery.ui.order.details

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.delivery.domain.orders.details.ChangeOrderStatusUseCase
import com.example.delivery.domain.orders.details.GetOrderAvailableActionListUseCase
import com.example.delivery.entities.*
import com.example.delivery.result.Event
import com.example.delivery.result.Result
import com.example.delivery.ui.location.LocationActivity
import com.example.delivery.utils.map
import com.example.delivery.utils.observeDataFrom
import com.example.delivery.utils.observeErrorFrom
import com.example.delivery.utils.switchMap
import javax.inject.Inject

/**
 * Created by Mohamed Fadel
 * Date: 11/1/2019.
 * email: mohamedfadel91@gmail.com.
 */
class OrderActionsViewModel @Inject constructor(
    private val info: OrderInfo,
    private val GetOrderAvailableActionListUseCase: GetOrderAvailableActionListUseCase,
    private val ChangeOrderStatusUseCase: ChangeOrderStatusUseCase
) :
    ViewModel() {

    val loading: LiveData<Boolean>

    val showBeingDeliveredButton: LiveData<Boolean>

    val showDeliveredButton: LiveData<Boolean>

    private val _errorMessage = MediatorLiveData<Event<String>>()
    val errorMessage: LiveData<Event<String>>
        get() = _errorMessage

    private val _successMessage = MediatorLiveData<Event<String>>()
    val successMessage: LiveData<Event<String>>
        get() = _successMessage

    private val orderIdParam = MutableLiveData<Int>()
    private val orderStatusParam = MutableLiveData<OrderAction>()

    private val _navigateTo = MediatorLiveData<Event<Navigation>>()
    val navigateTo: LiveData<Event<Navigation>>
        get() = _navigateTo

    private val loadOrderActionList =
        orderIdParam.switchMap { id ->
            GetOrderAvailableActionListUseCase.execute(id)
            GetOrderAvailableActionListUseCase.observe()
        }

    private val changeOrderStatus =
        orderStatusParam.switchMap {
            ChangeOrderStatusUseCase.execute(it)
            ChangeOrderStatusUseCase.observe()
        }


    init {
        loading = changeOrderStatus.map { it is Result.Loading }
        _errorMessage.observeErrorFrom(changeOrderStatus)
        _navigateTo.observeDataFrom(changeOrderStatus) {
            Event(Navigation(Destination.BACK))
        }
        _successMessage.observeDataFrom(changeOrderStatus){
            Event("Order Status Updated")
        }


        _errorMessage.observeErrorFrom(loadOrderActionList)

        showBeingDeliveredButton = loadOrderActionList.map {
            // TODO:: to adjust it later to remove business param from here..
            ((it as? Result.Success)?.data?.any { item -> item.value == OrderStatusEnum.BEING_DELIVERED.value }) ?: false
        }
        showDeliveredButton = loadOrderActionList.map {
            // TODO:: to adjust it later to remove business param from here..
            ((it as? Result.Success)?.data?.any { item -> item.value == OrderStatusEnum.DELIVERED.value }) ?: false
        }

        orderIdParam.postValue(info.order.id)

    }


    fun onBeingDeliveredButtonClicked() {
        orderStatusParam.postValue(OrderAction(info.order.id, 11))
    }

    fun onDeliveredButtonClicked() {
        _navigateTo.postValue(Event(Navigation(Destination.LOCATION, Bundle().apply {
            putInt(LocationActivity.EXTRA_ADDRESS_ID, info.order.user_address_id)
            putParcelable(LocationActivity.EXTRA_OLD_LOCATION, info.orderLocation)
        })))
    }

    fun onDeliveredButtonDialogClicked(){
        orderStatusParam.postValue(OrderAction(info.order.id, 12))
    }

}

data class Navigation(val dest: Destination, val extras: Bundle? = null)
enum class Destination {
    LOCATION, BACK
}
