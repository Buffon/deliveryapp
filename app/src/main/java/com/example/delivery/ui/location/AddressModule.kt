package com.example.delivery.ui.location

import com.example.delivery.di.scope.ActivityScoped
import com.example.delivery.domain.address.AddressRepository
import com.example.delivery.repo.address.AddressApi
import com.example.delivery.repo.address.AddressRepositoryImp
import dagger.Module
import dagger.Provides

/**
 * Created by Mohamed Fadel
 * Date: 10/28/2019.
 * email: mohamedfadel91@gmail.com.
 */
@Module
class AddressModule {

    @ActivityScoped
    @Provides
    fun provideAddressRepository(addressApi: AddressApi): AddressRepository {
        return AddressRepositoryImp(addressApi)
    }

}