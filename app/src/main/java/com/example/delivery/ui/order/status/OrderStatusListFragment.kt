package com.example.delivery.ui.order.status


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.delivery.databinding.FragmentOrderStatusListBinding
import com.example.delivery.ui.order.list.OrderListFragment
import com.example.delivery.utils.showErrorMessage
import com.example.delivery.utils.viewModelProvider
import com.example.delivery.entities.OrderStatus
import dagger.android.support.DaggerFragment
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class OrderStatusListFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var orderStatusListViewModel: OrderStatusListViewModel

    private lateinit var binding: FragmentOrderStatusListBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        orderStatusListViewModel = viewModelProvider(viewModelFactory)

        binding = FragmentOrderStatusListBinding.inflate(inflater, container, false).apply {
            viewModel = orderStatusListViewModel
            lifecycleOwner = viewLifecycleOwner
        }

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        orderStatusListViewModel.loadOrderStatusListResult.observe(this, Observer { orderStatusList ->
            binding.pager.adapter = OrdersListAdapter(childFragmentManager, orderStatusList)
            binding.tabsLayout.setupWithViewPager(binding.pager)
        })

        orderStatusListViewModel.errorMessage.showErrorMessage(activity!!)

    }


}

class OrdersListAdapter(
    fm: FragmentManager,
    private val orderStatusList: List<OrderStatus>
) : FragmentPagerAdapter(fm) {
    override fun getCount(): Int = orderStatusList.size

    override fun getItem(position: Int): Fragment =
        OrderListFragment.newInstance(orderStatusList[position])

    override fun getPageTitle(position: Int): CharSequence? = orderStatusList[position].name


}
