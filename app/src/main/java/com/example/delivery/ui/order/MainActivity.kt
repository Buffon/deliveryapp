package com.example.delivery.ui.order

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.delivery.R
import com.example.delivery.fcm.TopicSubscriber
import com.example.delivery.ui.order.status.OrderStatusListViewModel
import com.example.delivery.utils.viewModelProvider
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject lateinit var topicSubscriber: TopicSubscriber

    private lateinit var viewModel: OrderStatusListViewModel

    private var menu: Menu? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        topicSubscriber.subscribeToOrdersUpdates()

        viewModel = viewModelProvider(viewModelFactory)
        viewModel.loading.observe(this, Observer {
            menu?.findItem(R.id.refresh)?.isVisible = !it
        })

        setSupportActionBar(toolbar)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_toolbar, menu)
        this.menu = menu
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.refresh -> viewModel.onRefreshButtonClicked()
        }

        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == REQUEST_DETAILS_CODE){
            if(resultCode == RESULT_OK){
                viewModel.onOrderStatusChanged()
            }
        }

    }

    companion object{
        const val REQUEST_DETAILS_CODE = 0x01
    }
}
