package com.example.delivery.ui.order.list

import com.example.delivery.entities.OrderInfo

/**
 * Created by Mohamed Fadel
 * Date: 11/16/2019.
 * email: mohamedfadel91@gmail.com.
 */
data class OrderInfoItem(
    val orderInfos: List<OrderInfo>,
    val isHeaderShown: Boolean
)