package com.example.delivery.ui.location


import android.app.ProgressDialog
import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.delivery.R
import com.example.delivery.databinding.FragmentUpdateLocationBinding
import com.example.delivery.result.EventObserver
import com.example.delivery.utils.viewModelProvider
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import dagger.android.support.DaggerFragment
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 */
class UpdateLocationFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var locationViewModel: LocationViewModel

    private lateinit var binding: FragmentUpdateLocationBinding

    private lateinit var progressDialog: ProgressDialog

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        locationViewModel = viewModelProvider(viewModelFactory)

        binding = FragmentUpdateLocationBinding.inflate(inflater, container, false).apply {
            viewModel = locationViewModel
            lifecycleOwner = viewLifecycleOwner
        }

        progressDialog = ProgressDialog(context!!).apply {
            setMessage(getString(R.string.loading))
        }
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        locationViewModel.isLoading.observe(this, Observer {
            progressDialog.apply {
                if (it) show() else hide()
            }
        })

        val mapFragment = childFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment

        mapFragment.getMapAsync { maps ->
            maps.isMyLocationEnabled = true
            maps.setOnMapLongClickListener {
                maps.apply {
                    clear()
                    addMarker(MarkerOptions().position(it))
                }
                locationViewModel.updateLocation(it)

            }

            lifecycle.addObserver(MapInitialZoomToCurrentLocationHandler(maps))
            locationViewModel.oldLocation.observe(this, EventObserver{
                maps.addMarker(MarkerOptions().position(LatLng(it.latitude!!, it.longitude!!)))
            })
        }
    }

}
