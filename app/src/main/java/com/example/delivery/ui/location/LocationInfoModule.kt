package com.example.delivery.ui.location

import com.example.delivery.di.scope.ActivityScoped
import com.example.delivery.entities.Location
import dagger.Module
import dagger.Provides

/**
 * Created by Mohamed Fadel
 * Date: 11/2/2019.
 * email: mohamedfadel91@gmail.com.
 */
@Module
class LocationInfoModule {

    @Provides
    @ActivityScoped
    fun provideOrderInfo(locationActivity: LocationActivity): Int {
        return locationActivity.intent.extras?.getInt(LocationActivity.EXTRA_ADDRESS_ID)!!
    }

    @Provides
    @ActivityScoped
    fun provideAddressLocation(locationActivity: LocationActivity): Location? {
        return locationActivity.intent.extras?.getParcelable(LocationActivity.EXTRA_OLD_LOCATION)
    }
}