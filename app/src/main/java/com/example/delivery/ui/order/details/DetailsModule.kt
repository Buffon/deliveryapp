package com.example.delivery.ui.order.details

import com.example.delivery.di.scope.FragmentScoped
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by Mohamed Fadel
 * Date: 10/27/2019.
 * email: mohamedfadel91@gmail.com.
 */
@Module
internal abstract class DetailsModule {

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun contributeOrderDetailsFragment(): OrderDetailsFragment

}