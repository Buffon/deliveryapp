package com.example.delivery.ui.location

import androidx.lifecycle.ViewModel
import com.example.delivery.di.ViewModelKey
import com.example.delivery.di.scope.FragmentScoped
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

/**
 * Created by Mohamed Fadel
 * Date: 11/3/2019.
 * email: mohamedfadel91@gmail.com.
 */
@Module
internal abstract class UpdateLocationModule{

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun contributeUpdateLocationFragment(): UpdateLocationFragment
}