package com.example.delivery.ui.order.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.delivery.domain.orders.details.GetOrderAvailableActionListUseCase
import com.example.delivery.domain.orders.details.GetOrderItemListUseCase
import com.example.delivery.entities.OrderInfo
import com.example.delivery.entities.OrderItem
import com.example.delivery.result.Event
import com.example.delivery.result.Result
import com.example.delivery.utils.map
import com.example.delivery.utils.observeDataFrom
import com.example.delivery.utils.observeErrorFrom
import com.example.delivery.utils.switchMap
import javax.inject.Inject

/**
 * Created by Mohamed Fadel
 * Date: 11/1/2019.
 * email: mohamedfadel91@gmail.com.
 */
class OrderItemsViewModel @Inject constructor(
    private val info: OrderInfo,
    private val GetOrderItemListUseCase: GetOrderItemListUseCase
) :
    ViewModel() {
    private val loadOrderInfo: OrderInfo = info

    private val _loadOrderInfoResult = MutableLiveData<OrderInfo>()

    val loadOrderInfoResult: LiveData<OrderInfo>
        get() = _loadOrderInfoResult

    val loading: LiveData<Boolean>

    val isEmpty: LiveData<Boolean>

    private val _errorMessage = MediatorLiveData<Event<String>>()

    val errorMessage: LiveData<Event<String>>
        get() = _errorMessage

    private val _loadOrderItemListResult = MediatorLiveData<List<OrderItem>>()

    val loadOrderItemListResult: LiveData<List<OrderItem>>
        get() = _loadOrderItemListResult

    private val orderIdParam = MutableLiveData<Int>()

    private val loadOrderItemList =
        orderIdParam.switchMap { id ->
            GetOrderItemListUseCase.execute(id)
            GetOrderItemListUseCase.observe()
        }

    init {
        _loadOrderInfoResult.postValue(loadOrderInfo)

        loading = loadOrderItemList.map { it is Result.Loading }
        isEmpty = loadOrderItemList.map {
            it !is Result.Loading && ((it as? Result.Success)?.data.isNullOrEmpty() ?: true)
        }
        _errorMessage.observeErrorFrom(loadOrderItemList)
        _loadOrderItemListResult.observeDataFrom(loadOrderItemList)


        orderIdParam.postValue(info.order.id)
    }
}