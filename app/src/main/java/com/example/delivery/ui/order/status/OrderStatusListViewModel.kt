package com.example.delivery.ui.order.status

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import com.example.delivery.domain.orders.status.GetOrderStatusListUseCase
import com.example.delivery.result.Event
import com.example.delivery.result.Result
import com.example.delivery.utils.map
import com.example.delivery.utils.observeDataFrom
import com.example.delivery.utils.observeErrorFrom
import com.example.delivery.entities.OrderStatus
import javax.inject.Inject

/**
 * Created by Mohamed Fadel
 * Date: 10/27/2019.
 * email: mohamedfadel91@gmail.com.
 */
class OrderStatusListViewModel @Inject constructor(private val getOrderStatusListUseCase: GetOrderStatusListUseCase) :
    ViewModel() {

    val loading: LiveData<Boolean>

    private val _errorMessage = MediatorLiveData<Event<String>>()

    val errorMessage: LiveData<Event<String>>
        get() = _errorMessage

    private val _loadOrderStatusListResult = MediatorLiveData<List<OrderStatus>>()

    val loadOrderStatusListResult: LiveData<List<OrderStatus>>
        get() = _loadOrderStatusListResult

    init {
        val observableAgenda = getOrderStatusListUseCase.observe()
        getOrderStatusList()

        loading = observableAgenda.map { it is Result.Loading }

        _errorMessage.observeErrorFrom(observableAgenda)

        _loadOrderStatusListResult.observeDataFrom(observableAgenda)

    }

    fun onRefreshButtonClicked() {
        getOrderStatusList()
    }

    fun onOrderStatusChanged(){
        getOrderStatusList()
    }

    private fun getOrderStatusList() {
        getOrderStatusListUseCase.execute(Unit)

    }

}


