package com.example.delivery.ui.order.details

import android.os.Bundle
import com.example.delivery.di.scope.ActivityScoped
import com.example.delivery.entities.OrderInfo
import dagger.Module
import dagger.Provides

/**
 * Created by Mohamed Fadel
 * Date: 11/2/2019.
 * email: mohamedfadel91@gmail.com.
 */
@Module
class OrderInfoModule {

    @Provides
    @ActivityScoped
    fun provideOrderInfo(detailsActivity: DetailsActivity): OrderInfo {
        return detailsActivity.intent.extras!!.getParcelable(DetailsActivity.EXTRA_ORDER_INFO)
    }
}