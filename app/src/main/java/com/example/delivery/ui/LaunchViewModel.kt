package com.example.delivery.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import com.example.delivery.domain.login.GetAuthUseCase
import com.example.delivery.result.Event
import com.example.delivery.result.Result
import com.example.delivery.utils.observeDataFrom
import javax.inject.Inject

/**
 * Created by Mohamed Fadel
 * Date: 10/28/2019.
 * email: mohamedfadel91@gmail.com.
 */
class LaunchViewModel @Inject constructor(private val GetAuthUseCase: GetAuthUseCase) :
    ViewModel() {

    private val _navigateTo = MediatorLiveData<Event<Destination>>()
    val navigateTo: LiveData<Event<Destination>>
        get() = _navigateTo

    private val getAuth: LiveData<Result<String?>> = GetAuthUseCase.invoke(Unit)


    fun OnNotAllPermissionsGranted() {
        _navigateTo.postValue(Event(Destination.EXIT))
    }

    fun OnAllPermissionsGranted(){
        _navigateTo.observeDataFrom(getAuth) {
            if (it != null) {
                Event(Destination.MAIN)
            } else {
                Event(Destination.LOGIN)
            }
        }
    }

}

enum class Destination {
    MAIN, LOGIN, EXIT
}