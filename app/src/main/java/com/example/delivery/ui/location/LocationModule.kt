package com.example.delivery.ui.location

import androidx.lifecycle.ViewModel
import com.example.delivery.di.ViewModelKey
import com.example.delivery.di.scope.ActivityScoped
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Created by Mohamed Fadel
 * Date: 11/3/2019.
 * email: mohamedfadel91@gmail.com.
 */
@Module
internal abstract class LocationModule{

    @ActivityScoped
    @Binds
    @IntoMap
    @ViewModelKey(LocationViewModel::class)
    internal abstract fun bindLocationViewModel(viewModel: LocationViewModel): ViewModel
}