package com.example.delivery.ui.order

import com.example.delivery.di.scope.ActivityScoped
import com.example.delivery.domain.orders.OrdersRepository
import com.example.delivery.repo.order.OrderApi
import com.example.delivery.repo.order.OrdersRepositoryImp
import com.example.delivery.repo.prefs.PreferenceStorage
import dagger.Module
import dagger.Provides

/**
 * Created by Mohamed Fadel
 * Date: 10/28/2019.
 * email: mohamedfadel91@gmail.com.
 */
@Module
class OrderModule {

    @ActivityScoped
    @Provides
    fun provideOrdersRepository(orderApi: OrderApi, preferenceStorage: PreferenceStorage): OrdersRepository {
        return OrdersRepositoryImp(orderApi, preferenceStorage)
    }

}