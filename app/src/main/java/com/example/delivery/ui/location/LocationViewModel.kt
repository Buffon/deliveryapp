package com.example.delivery.ui.location

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.delivery.domain.address.AddressUpdateParam
import com.example.delivery.domain.address.UpdateAddressLocationUseCase
import com.example.delivery.entities.Location
import com.example.delivery.result.Event
import com.example.delivery.result.Result
import com.example.delivery.utils.map
import com.example.delivery.utils.observeDataFrom
import com.example.delivery.utils.observeErrorFrom
import com.example.delivery.utils.switchMap
import com.google.android.gms.maps.model.LatLng
import javax.inject.Inject

/**
 * Created by Mohamed Fadel
 * Date: 11/3/2019.
 * email: mohamedfadel91@gmail.com.
 */
class LocationViewModel @Inject constructor(private val addressId: Int,
                                            private val addressOldLocation: Location?,
                                            private val UpdateAddressLocationUseCase: UpdateAddressLocationUseCase) :
    ViewModel() {

    private val selectedLocation = MutableLiveData<Location>()
    private val _oldLocation = MutableLiveData<Event<Location>>()
    val oldLocation: LiveData<Event<Location>>
    get() = _oldLocation

    val isLocationSelected: LiveData<Boolean>

    private val _navigateTo = MediatorLiveData<Event<Destination>>()
    val navigateTo: LiveData<Event<Destination>>
        get() = _navigateTo


    private val addressUpdateParam = MutableLiveData<AddressUpdateParam>()

    private val updateLocation = addressUpdateParam.switchMap {
        UpdateAddressLocationUseCase.execute(it)
        UpdateAddressLocationUseCase.observe()
    }
    val isLoading: LiveData<Boolean>

    private val _errorMessage = MediatorLiveData<Event<String>>()
    val errorMessage: LiveData<Event<String>>
        get() = _errorMessage

    private val _updateLocationResult = MediatorLiveData<Event<String>>()
    val updateLocationResult: LiveData<Event<String>>
        get() = _updateLocationResult

    init {
        isLoading = updateLocation.map { it is Result.Loading }

        _errorMessage.observeErrorFrom(updateLocation)
        _updateLocationResult.observeDataFrom(updateLocation) {
            Event("Location Updated")
        }

        _navigateTo.observeDataFrom(updateLocation){
            Event(Destination.DETAILS)
        }

        if(addressOldLocation!=null) {
            _oldLocation.postValue(Event(addressOldLocation))
        }



    }


    init {
        isLocationSelected = selectedLocation.map { it != null }
    }

    fun updateLocation(latLng: LatLng) {
        selectedLocation.postValue(Location(latLng.latitude, latLng.longitude))
    }

    fun onSkipButtonClicked() {
        _navigateTo.postValue(Event(Destination.DETAILS))
    }

    fun onUpdateButtonClicked() {
        addressUpdateParam.postValue(AddressUpdateParam(selectedLocation.value!!, addressId))
    }
}

enum class Destination {
    DETAILS
}