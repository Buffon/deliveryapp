package com.example.delivery.ui.order.details

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.delivery.R
import com.example.delivery.entities.Location
import com.example.delivery.entities.OrderInfo
import com.example.delivery.utils.handlePhoneCall
import com.example.delivery.utils.startNavigationTo
import com.example.delivery.utils.viewModelProvider
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_details.*
import javax.inject.Inject


class DetailsActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var phoneNum: String

    private var location: Location? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        val viewModel: OrderItemsViewModel = viewModelProvider(viewModelFactory)

        viewModel.loadOrderInfoResult.observe(this, Observer {
            this.phoneNum = it.info.mobile
            this.location = it.orderLocation

            invalidateOptionsMenu()
        })

        setSupportActionBar(toolbar)

        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(if(location != null) R.menu.details_toolbar_nav else R.menu.details_toolbar, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.call -> handlePhoneCall(phoneNum)
            R.id.navigate -> startNavigationTo(location!!)
        }

        return super.onOptionsItemSelected(item)
    }

    companion object {
        const val EXTRA_ORDER_INFO = "orderInfo"

        @JvmStatic
        fun buildIntent(context: Context, info: OrderInfo) =
            Intent(context, DetailsActivity::class.java).apply {
                putExtra(EXTRA_ORDER_INFO, info)
            }
    }
}
