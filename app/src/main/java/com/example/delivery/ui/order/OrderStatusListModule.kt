package com.example.delivery.ui.order

import androidx.lifecycle.ViewModel
import com.example.delivery.di.ViewModelKey
import com.example.delivery.di.scope.ActivityScoped
import com.example.delivery.ui.order.status.OrderStatusListViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Created by Mohamed Fadel
 * Date: 10/28/2019.
 * email: mohamedfadel91@gmail.com.
 */
@Module
internal abstract class OrderStatusListModule {
    /**
     * The ViewModels are created by Dagger in a map. Via the @ViewModelKey, we define that we
     * want to get a [OrderStatusListViewModel] class.
     */
    @Binds
    @IntoMap
    @ActivityScoped
    @ViewModelKey(OrderStatusListViewModel::class)
    internal abstract fun bindOrderListViewModel(viewModel: OrderStatusListViewModel): ViewModel
}