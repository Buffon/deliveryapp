package com.example.delivery.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.delivery.domain.login.CacheAuthUseCase
import com.example.delivery.result.Event
import com.example.delivery.utils.observeDataFrom
import com.example.delivery.utils.switchMap
import javax.inject.Inject

/**
 * Created by Mohamed Fadel
 * Date: 11/5/2019.
 * email: mohamedfadel91@gmail.com.
 */
class LoginViewModel @Inject constructor(private val CacheAuthUseCase: CacheAuthUseCase) :
    ViewModel() {

    private val hash = MutableLiveData<String>()

    private val _navigateTo = MediatorLiveData<Event<Destination>>()
    val navigateTo: LiveData<Event<Destination>>
        get() = _navigateTo

    private val text = MutableLiveData<String>()

    private val cacheText = text.switchMap {
        CacheAuthUseCase.invoke(it)
    }

    init {
        _navigateTo.observeDataFrom(cacheText){
            Event(Destination.MAIN)
        }
    }

    fun OnTextChanged(text: String) {
        hash.postValue(text)
    }

    fun OnLoginButtonClicked() {
        text.value = hash.value
    }
}

enum class Destination {
    MAIN
}