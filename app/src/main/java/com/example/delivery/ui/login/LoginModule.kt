package com.example.delivery.ui.login

import androidx.lifecycle.ViewModel
import com.example.delivery.di.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Created by Mohamed Fadel
 * Date: 10/28/2019.
 * email: mohamedfadel91@gmail.com.
 */
@Module
internal abstract class LoginModule{

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    internal abstract fun bindLoginViewModel(viewModel: LoginViewModel): ViewModel
}