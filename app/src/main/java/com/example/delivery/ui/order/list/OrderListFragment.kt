package com.example.delivery.ui.order.list


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.delivery.databinding.FragmentOrderListBinding
import com.example.delivery.entities.OrderStatus
import com.example.delivery.utils.showErrorMessage
import com.example.delivery.utils.viewModelProvider
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_order_list.*
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class OrderListFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var orderListViewModel: OrderListViewModel

    private lateinit var binding: FragmentOrderListBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        orderListViewModel = viewModelProvider(viewModelFactory)

        binding = FragmentOrderListBinding.inflate(inflater, container, false).apply {
            viewModel = orderListViewModel
            lifecycleOwner = viewLifecycleOwner
        }

        return binding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        orderListViewModel.loadOrderInfoListResult.observe(this, Observer {
            if (list.adapter == null) {
                list.adapter = OrderListAdapter(it.isHeaderShown)
            }
            (list.adapter as OrderListAdapter).apply {
                submitOrderInfoList(it.orderInfos)
            }
        })

        orderListViewModel.errorMessage.showErrorMessage(activity!!)

        orderListViewModel.onScreenStarted((arguments?.getParcelable(EXTRA_ORDER_STATUS) as? OrderStatus)!!)

    }

    companion object {

        private const val EXTRA_ORDER_STATUS = "orderStatus"

        @JvmStatic
        fun newInstance(orderStatus: OrderStatus) = OrderListFragment().apply {
            arguments = Bundle().apply {
                putParcelable(EXTRA_ORDER_STATUS, orderStatus)
            }
        }
    }


}


