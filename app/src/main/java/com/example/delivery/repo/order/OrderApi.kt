package com.example.delivery.repo.order

import androidx.lifecycle.LiveData
import com.example.delivery.entities.OrderAction
import com.example.delivery.entities.OrderInfo
import com.example.delivery.entities.OrderItem
import com.example.delivery.entities.OrderStatus
import com.example.delivery.repo.ApiResponse
import com.example.delivery.repo.ResponseWrapper

/**
 * Created by Mohamed Fadel
 * Date: 10/28/2019.
 * email: mohamedfadel91@gmail.com.
 */
interface OrderApi {
    fun getOrderStatusList(): LiveData<ApiResponse<ResponseWrapper<List<OrderStatus>>>>
    fun getOrderActionList(orderId: Int): LiveData<ApiResponse<ResponseWrapper<List<OrderStatus>>>>
    fun getOrderList(
        auth: String?,
        orderStatus: Int
    ): LiveData<ApiResponse<ResponseWrapper<List<OrderInfo>>>>

    fun getOrderItemList(
        auth: String?,
        orderId: Int
    ): LiveData<ApiResponse<ResponseWrapper<List<OrderItem>>>>

    fun executeOrderAction(
        auth: String?,
        orderId: Int,
        orderStatus: Int
    ): LiveData<ApiResponse<ResponseWrapper<Any>>>
}