package com.example.delivery.repo.address

import androidx.lifecycle.LiveData
import com.example.delivery.entities.Location
import com.example.delivery.entities.OrderInfo
import com.example.delivery.entities.OrderItem
import com.example.delivery.entities.OrderStatus
import com.example.delivery.repo.ApiResponse
import com.example.delivery.repo.ResponseWrapper

/**
 * Created by Mohamed Fadel
 * Date: 10/28/2019.
 * email: mohamedfadel91@gmail.com.
 */
interface AddressApi {
    fun updateAddressLocation(addressId: Int, location: Location): LiveData<ApiResponse<ResponseWrapper<Any>>>
}