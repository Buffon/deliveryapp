package com.example.delivery.repo.order

import androidx.lifecycle.LiveData
import com.example.delivery.domain.orders.OrdersRepository
import com.example.delivery.entities.OrderAction
import com.example.delivery.entities.OrderInfo
import com.example.delivery.entities.OrderItem
import com.example.delivery.entities.OrderStatus
import com.example.delivery.repo.ApiResponse
import com.example.delivery.repo.NetworkBoundResource
import com.example.delivery.repo.ResponseWrapper
import com.example.delivery.repo.prefs.PreferenceStorage
import com.example.delivery.result.Result
import javax.inject.Inject

/**
 * Created by Mohamed Fadel
 * Date: 10/27/2019.
 * email: mohamedfadel91@gmail.com.
 */

class OrdersRepositoryImp @Inject constructor(private val orderApi: OrderApi,
                                              private val preferenceStorage: PreferenceStorage) : OrdersRepository {
    override fun getOrderStatus(): LiveData<Result<List<OrderStatus>>> {
        return object : NetworkBoundResource<List<OrderStatus>>() {
            override fun createCall(): LiveData<ApiResponse<ResponseWrapper<List<OrderStatus>>>> {
                return orderApi.getOrderStatusList()
            }

        }.asLiveData()

    }

    override fun getOrderList(orderStatus: Int): LiveData<Result<List<OrderInfo>>> {
        return object : NetworkBoundResource<List<OrderInfo>>() {
            override fun createCall(): LiveData<ApiResponse<ResponseWrapper<List<OrderInfo>>>> {
                return orderApi.getOrderList(preferenceStorage.adminAuth, orderStatus)
            }

        }.asLiveData()
    }

    override fun getOrderItemList(orderId: Int): LiveData<Result<List<OrderItem>>> {
        return object : NetworkBoundResource<List<OrderItem>>() {
            override fun createCall(): LiveData<ApiResponse<ResponseWrapper<List<OrderItem>>>> {
                return orderApi.getOrderItemList(preferenceStorage.adminAuth, orderId)
            }

        }.asLiveData()
    }

    override fun getOrderActionList(orderId: Int): LiveData<Result<List<OrderStatus>>> {
        return object : NetworkBoundResource<List<OrderStatus>>() {
            override fun createCall(): LiveData<ApiResponse<ResponseWrapper<List<OrderStatus>>>> {
                return orderApi.getOrderActionList(orderId)
            }

        }.asLiveData()
    }

    override fun executeOrderAction(orderId: Int, orderStatus: Int): LiveData<Result<Any>> {
        return object : NetworkBoundResource<Any>() {
            override fun createCall(): LiveData<ApiResponse<ResponseWrapper<Any>>> {
                return orderApi.executeOrderAction(preferenceStorage.adminAuth, orderId, orderStatus)
            }

        }.asLiveData()
    }
}