package com.example.delivery.repo

/**
 * Created by Mohamed Fadel
 * Date: 10/27/2019.
 * email: mohamedfadel91@gmail.com.
 */
data class ResponseWrapper<out T>(
    val data: T?,
    val status: Int,
    val message: String?,
    val errorCode: Int = 0
)