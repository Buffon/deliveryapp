package com.example.delivery.repo.address

import androidx.lifecycle.LiveData
import com.example.delivery.domain.address.AddressRepository
import com.example.delivery.entities.Location
import com.example.delivery.repo.ApiResponse
import com.example.delivery.repo.NetworkBoundResource
import com.example.delivery.repo.ResponseWrapper
import com.example.delivery.result.Result
import javax.inject.Inject

/**
 * Created by Mohamed Fadel
 * Date: 10/27/2019.
 * email: mohamedfadel91@gmail.com.
 */

class AddressRepositoryImp @Inject constructor(private val addressApi: AddressApi) :
    AddressRepository {
    override fun updateAddressLocation(addressId: Int, location: Location): LiveData<Result<Any>> {
        return object : NetworkBoundResource<Any>() {
            override fun createCall(): LiveData<ApiResponse<ResponseWrapper<Any>>> {
                return addressApi.updateAddressLocation(addressId, location)
            }

        }.asLiveData()
    }
}