package com.example.delivery.di.scope

import javax.inject.Scope
import kotlin.annotation.Retention
import kotlin.annotation.Target
import kotlin.annotation.AnnotationRetention

/**
 * The BroadcastReceiverScoped custom scoping annotation specifies that the lifespan of a dependency be
 * the same as that of a BroadcastReceiver. This is used to annotate dependencies that behave like a
 * singleton within the lifespan of a BroadcastReceiver.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.TYPE, AnnotationTarget.CLASS, AnnotationTarget.FUNCTION)
annotation class BroadcastReceiverScoped
