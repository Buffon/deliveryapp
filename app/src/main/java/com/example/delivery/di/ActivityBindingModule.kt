package com.example.delivery.di

import com.example.delivery.di.scope.ActivityScoped
import com.example.delivery.ui.LaunchModule
import com.example.delivery.ui.LauncherActivity
import com.example.delivery.ui.location.*
import com.example.delivery.ui.login.LoginActivity
import com.example.delivery.ui.login.LoginModule
import com.example.delivery.ui.order.MainActivity
import com.example.delivery.ui.order.MainModule
import com.example.delivery.ui.order.OrderModule
import com.example.delivery.ui.order.OrderStatusListModule
import com.example.delivery.ui.order.details.*
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by Mohamed Fadel
 * Date: 10/27/2019.
 * email: mohamedfadel91@gmail.com.
 */
@Module
abstract class ActivityBindingModule {

    @ActivityScoped
    @ContributesAndroidInjector(modules = [LaunchModule::class])
    internal abstract fun launcherActivity(): LauncherActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = [LoginModule::class])
    internal abstract fun loginActivity(): LoginActivity

    @ActivityScoped
    @ContributesAndroidInjector(
        modules = [MainModule::class,
            OrderModule::class,
            OrderStatusListModule::class]
    )
    internal abstract fun mainActivity(): MainActivity

    @ActivityScoped
    @ContributesAndroidInjector(
        modules = [DetailsModule::class,
            OrderItemsModule::class,
            OrderModule::class,
            OrderActionsModule::class,
            OrderInfoModule::class]
    )
    internal abstract fun detailsActivity(): DetailsActivity

    @ActivityScoped
    @ContributesAndroidInjector(
        modules = [LocationModule::class,
            LocationInfoModule::class,
            AddressModule::class,
            UpdateLocationModule::class]
    )
    internal abstract fun updateLocationActivity(): LocationActivity
}