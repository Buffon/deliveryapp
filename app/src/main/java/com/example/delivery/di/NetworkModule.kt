package com.example.delivery.di

import com.example.delivery.BuildConfig
import com.example.delivery.api.*
import com.example.delivery.repo.address.AddressApi
import com.example.delivery.repo.order.OrderApi
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level.BODY
import javax.inject.Singleton

/**
 * Created by Mohamed Fadel
 * Date: 10/28/2019.
 * email: mohamedfadel91@gmail.com.
 */

@Module
class NetworkModule {

    @Singleton
    @Provides
    fun provideOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder().apply {
            if (BuildConfig.DEBUG) {
                addInterceptor(HttpLoggingInterceptor().apply {
                    level = BODY
                })
            }
        }.build()
    }

    @Singleton
    @Provides
    fun provideGson(): Gson {
        return GsonBuilder().setDateFormat("yyyy-MM-dd'T'hh:mm:ss").create()
    }

    @Singleton
    @Provides
    fun provideRetrofitOrderApi(okHttpClient: OkHttpClient, gson: Gson): OrderApiSpec {
        return createService(
            BuildConfig.BASE_URL,
            okHttpClient, gson
        )
    }

    @Singleton
    @Provides
    fun provideOrderApi(orderApiSpec: OrderApiSpec): OrderApi {
        return RetrofitOrderApi(orderApiSpec)
    }

    @Singleton
    @Provides
    fun provideRetrofitAddressApi(okHttpClient: OkHttpClient, gson: Gson): AddressApiSpec {
        return createService(
            BuildConfig.BASE_URL,
            okHttpClient, gson
        )
    }

    @Singleton
    @Provides
    fun provideAddressApi(addressApiSpec: AddressApiSpec): AddressApi {
        return RetrofitAddressApi(addressApiSpec)
    }
}
