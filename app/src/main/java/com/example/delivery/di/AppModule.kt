package com.example.delivery.di

import android.content.Context
import android.net.ConnectivityManager
import com.example.delivery.MainApplication
import com.example.delivery.domain.internal.AppHandler
import com.example.delivery.domain.internal.AppMainHandler
import com.example.delivery.fcm.FcmTopicSubscriber
import com.example.delivery.fcm.TopicSubscriber
import com.example.delivery.repo.prefs.PreferenceStorage
import com.example.delivery.repo.prefs.SharedPreferenceStorage
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Mohamed Fadel
 * Date: 10/27/2019.
 * email: mohamedfadel91@gmail.com.
 */
@Module
class AppModule {
    @Provides
    fun provideContext(application: MainApplication): Context {
        return application.applicationContext
    }

    @Singleton
    @Provides
    fun providesPreferenceStorage(context: Context): PreferenceStorage =
        SharedPreferenceStorage(context)

    @Provides
    fun providesConnectivityManager(context: Context): ConnectivityManager =
        context.applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager

    @Singleton
    @Provides
    @MainThreadHandler
    fun providesMainThreadHandler(): AppHandler = AppMainHandler()


    @Singleton
    @Provides
    fun providesTopicSubscriber() : TopicSubscriber{
        return FcmTopicSubscriber()
    }

}