package com.example.delivery.fcm

import com.google.firebase.messaging.FirebaseMessaging
import timber.log.Timber

/**
 * A [TopicSubscriber] that uses Firebase Cloud Messaging to subscribe a user to server topics.
 *
 * Calls are lightweight and can be repeated multiple times.
 */
class FcmTopicSubscriber : TopicSubscriber {

    override fun subscribeToOrdersUpdates() {
        try {
            FirebaseMessaging.getInstance().subscribeToTopic(ORDER_UPDATES_TOPIC_KEY)
        } catch (e: Exception) {
            Timber.e(e, "Error subscribing to order updates topic")
        }
    }

    companion object {
        private const val ORDER_UPDATES_TOPIC_KEY = "DeliveryApp"
    }
}
