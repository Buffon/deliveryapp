package com.example.delivery.fcm

/**
 * Used to subscribe users to server topics.
 */
interface TopicSubscriber {
    /**
     * Used to subscribe all users to general channel for delivery process..
     */
    fun subscribeToOrdersUpdates()
}