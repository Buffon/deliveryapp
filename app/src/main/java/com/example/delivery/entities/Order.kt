package com.example.delivery.entities

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

/**
 * Created by Mohamed Fadel
 * Date: 10/29/2019.
 * email: mohamedfadel91@gmail.com.
 */
@Parcelize
data class Order(
    val id: Int,
    val order_number: String,
    val user_id: Int,
    val user_address_id: Int,
    val order_subtotal: Double?,
    val selected_slot_date: String?,
    val selected_slot_time: String?,
    val order_status_id: Int,
    var orderStatus: OrderStatus
    ): Parcelable

@Parcelize
data class OrderInfo(
    val info: Info,
    val order: Order,
    val orderLocation : Location?,
    val orderTime: Date?
): Parcelable

@Parcelize
data class Info(
    val name: String,
    val mobile: String,
    val address_name: String,
    val address_details: String?,
    val region: String,
    val apartment: String
): Parcelable

@Parcelize
data class Location(
    val latitude: Double?,
    val longitude: Double?
): Parcelable