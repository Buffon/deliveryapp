package com.example.delivery.entities

/**
 * Created by Mohamed Fadel
 * Date: 11/2/2019.
 * email: mohamedfadel91@gmail.com.
 */
data class OrderAction(
    val order_id: Int,
    val order_status: Int
)