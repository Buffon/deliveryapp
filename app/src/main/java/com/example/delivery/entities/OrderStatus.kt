package com.example.delivery.entities

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OrderStatus(val value: Int, val name: String) : Parcelable

enum class OrderStatusEnum(val value: Int){
    NEW(10),
    BEING_DELIVERED(11),
    DELIVERED(12),
    CANCELED(30)
}

