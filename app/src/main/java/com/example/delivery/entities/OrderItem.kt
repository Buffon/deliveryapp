package com.example.delivery.entities

import kotlinx.android.parcel.Parcelize

/**
 * Created by Mohamed Fadel
 * Date: 11/2/2019.
 * email: mohamedfadel91@gmail.com.
 */
data class OrderItem(
    val product_name: String,
    val product_unit_name: String,
    val quantity: Double?,
    val price: Double?,
    val notes: String?
)