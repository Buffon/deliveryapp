package com.example.delivery.domain.orders.details

import com.example.delivery.domain.MediatorUseCase
import com.example.delivery.domain.orders.OrdersRepository
import com.example.delivery.entities.OrderStatus
import com.example.delivery.result.Result
import javax.inject.Inject

/**
 * Created by Mohamed Fadel
 * Date: 10/27/2019.
 * email: mohamedfadel91@gmail.com.
 */
class GetOrderAvailableActionListUseCase @Inject constructor(
    private val repository: OrdersRepository
) : MediatorUseCase<Int, List<OrderStatus>>() {

    override fun execute(parameters: Int) {
        try {
            val observableOrderList = repository.getOrderActionList(parameters)
            result.removeSource(observableOrderList)
            result.addSource(observableOrderList) {
                result.postValue(it)
            }
        } catch (e: Exception) {
            result.postValue(Result.Error(e.message ?: "unknown error"))
        }
    }


}

