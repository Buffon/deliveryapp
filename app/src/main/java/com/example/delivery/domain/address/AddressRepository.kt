package com.example.delivery.domain.address

import androidx.lifecycle.LiveData
import com.example.delivery.entities.Location
import com.example.delivery.result.Result

interface AddressRepository {
    fun updateAddressLocation(addressId: Int, location: Location): LiveData<Result<Any>>
}