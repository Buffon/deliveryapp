package com.example.delivery.domain.orders.details

import com.example.delivery.domain.MediatorUseCase
import com.example.delivery.domain.orders.OrdersRepository
import com.example.delivery.result.Result
import com.example.delivery.entities.OrderItem
import javax.inject.Inject

/**
 * Created by Mohamed Fadel
 * Date: 10/27/2019.
 * email: mohamedfadel91@gmail.com.
 */
class GetOrderItemListUseCase @Inject constructor(
    private val repository: OrdersRepository
) : MediatorUseCase<Int, List<OrderItem>>() {

    override fun execute(parameters: Int) {
        try {
            val observableOrderList = repository.getOrderItemList(parameters)
            result.removeSource(observableOrderList)
            result.addSource(observableOrderList) {
                result.postValue(it)
            }
        } catch (e: Exception) {
            result.postValue(Result.Error(e.message ?: "unknown error"))
        }
    }


}

