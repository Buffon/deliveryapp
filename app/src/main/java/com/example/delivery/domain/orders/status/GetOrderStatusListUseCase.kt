package com.example.delivery.domain.orders.status

import com.example.delivery.domain.MediatorUseCase
import com.example.delivery.domain.orders.OrdersRepository
import com.example.delivery.result.Result
import com.example.delivery.entities.OrderStatus
import javax.inject.Inject

/**
 * Created by Mohamed Fadel
 * Date: 10/27/2019.
 * email: mohamedfadel91@gmail.com.
 */
class GetOrderStatusListUseCase @Inject constructor(
    private val repository: OrdersRepository
) : MediatorUseCase<Unit, List<OrderStatus>>() {

    override fun execute(parameters: Unit) {
        try {
            val observableOrderStatus = repository.getOrderStatus()
            result.removeSource(observableOrderStatus)
            result.addSource(observableOrderStatus) {
                result.postValue(it)
            }
        } catch (e: Exception) {
            result.postValue(Result.Error(e.message ?: "unknown error"))
        }
    }


}

