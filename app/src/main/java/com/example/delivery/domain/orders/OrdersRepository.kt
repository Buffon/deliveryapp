package com.example.delivery.domain.orders

import androidx.lifecycle.LiveData
import com.example.delivery.result.Result
import com.example.delivery.entities.OrderInfo
import com.example.delivery.entities.OrderItem
import com.example.delivery.entities.OrderStatus

interface OrdersRepository {
    fun getOrderStatus(): LiveData<Result<List<OrderStatus>>>
    fun getOrderList(orderStatus: Int): LiveData<Result<List<OrderInfo>>>
    fun getOrderItemList(orderId: Int): LiveData<Result<List<OrderItem>>>
    fun getOrderActionList(orderId: Int): LiveData<Result<List<OrderStatus>>>
    fun executeOrderAction(orderId: Int, orderStatus: Int): LiveData<Result<Any>>
}