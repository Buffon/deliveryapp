package com.example.delivery.domain.orders.list

import com.example.delivery.domain.MediatorUseCase
import com.example.delivery.domain.orders.OrdersRepository
import com.example.delivery.entities.OrderInfo
import com.example.delivery.entities.OrderStatus
import com.example.delivery.entities.OrderStatusEnum
import com.example.delivery.result.Result
import javax.inject.Inject

/**
 * Created by Mohamed Fadel
 * Date: 10/27/2019.
 * email: mohamedfadel91@gmail.com.
 */
class GetOrderInfoListUseCase @Inject constructor(
    private val repository: OrdersRepository
) : MediatorUseCase<OrderStatus, List<OrderInfo>>() {

    override fun execute(parameters: OrderStatus) {
        try {
            val observableOrderList = repository.getOrderList(parameters.value)
            result.removeSource(observableOrderList)
            result.addSource(observableOrderList) {
                var resultData: Result<List<OrderInfo>> = it
                (it as? Result.Success)?.data?.apply {
                    forEach { info ->
                        info.order.orderStatus = parameters
                    }
                    if (parameters.value == OrderStatusEnum.NEW.value) {
                        resultData = Result.Success(sortedBy { order -> order.orderTime })
                    }
                }
                result.postValue(resultData)
            }
        } catch (e: Exception) {
            result.postValue(Result.Error(e.message ?: "unknown error"))
        }
    }


}

