package com.example.delivery.domain.address

import com.example.delivery.domain.MediatorUseCase
import com.example.delivery.result.Result
import javax.inject.Inject

/**
 * Created by Mohamed Fadel
 * Date: 10/27/2019.
 * email: mohamedfadel91@gmail.com.
 */
class UpdateAddressLocationUseCase @Inject constructor(
    private val repository: AddressRepository
) : MediatorUseCase<AddressUpdateParam, Any>() {

    override fun execute(parameters: AddressUpdateParam) {
        try {
            val observableOrderList =
                repository.updateAddressLocation(parameters.addressId, parameters.location)
            result.removeSource(observableOrderList)
            result.addSource(observableOrderList) {
                result.postValue(it)
            }
        } catch (e: Exception) {
            result.postValue(Result.Error(e.message ?: "unknown error"))
        }
    }


}



