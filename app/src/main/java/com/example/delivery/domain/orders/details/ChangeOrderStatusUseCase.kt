package com.example.delivery.domain.orders.details

import com.example.delivery.domain.MediatorUseCase
import com.example.delivery.domain.orders.OrdersRepository
import com.example.delivery.entities.OrderAction
import com.example.delivery.result.Result
import javax.inject.Inject

/**
 * Created by Mohamed Fadel
 * Date: 10/27/2019.
 * email: mohamedfadel91@gmail.com.
 */
class ChangeOrderStatusUseCase @Inject constructor(
    private val repository: OrdersRepository
) : MediatorUseCase<OrderAction, Any>() {

    override fun execute(parameters: OrderAction) {
        try {
            val observableOrderList = repository.executeOrderAction(parameters.order_id, parameters.order_status)
            result.removeSource(observableOrderList)
            result.addSource(observableOrderList) {
                result.postValue(it)
            }
        } catch (e: Exception) {
            result.postValue(Result.Error(e.message ?: "unknown error"))
        }
    }


}

