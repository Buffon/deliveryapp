package com.example.delivery.domain.login

import com.example.delivery.domain.UseCase
import com.example.delivery.repo.prefs.PreferenceStorage
import javax.inject.Inject

/**
 * Created by Mohamed Fadel
 * Date: 11/5/2019.
 * email: mohamedfadel91@gmail.com.
 */
class GetAuthUseCase @Inject constructor(private val preferenceStorage: PreferenceStorage)
    : UseCase<Unit,String?>(){

    override fun execute(parameters: Unit): String? {
        return preferenceStorage.adminAuth
    }

}