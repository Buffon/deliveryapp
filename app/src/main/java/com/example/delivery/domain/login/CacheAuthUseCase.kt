package com.example.delivery.domain.login

import com.example.delivery.domain.UseCase
import com.example.delivery.repo.prefs.PreferenceStorage
import javax.inject.Inject

/**
 * Created by Mohamed Fadel
 * Date: 11/5/2019.
 * email: mohamedfadel91@gmail.com.
 */
class CacheAuthUseCase @Inject constructor(private val preferenceStorage: PreferenceStorage)
    : UseCase<String, Unit>(){
    override fun execute(parameters: String) {
        preferenceStorage.adminAuth = parameters
    }

}