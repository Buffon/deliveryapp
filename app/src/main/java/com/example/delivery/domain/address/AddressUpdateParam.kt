package com.example.delivery.domain.address

import com.example.delivery.entities.Location


/**
 * Created by Mohamed Fadel
 * Date: 11/2/2019.
 * email: mohamedfadel91@gmail.com.
 */
data class AddressUpdateParam(
    val location: Location,
    val addressId: Int
)